import random
# imports should be placed at the top of the file
# they include a library into the file, so that it can be used by the programmer
# the random library has functions that are use for creating random events

# the . is used for going inside something and accessing what it contains
# we go inside random, access randint which is a function
# the function takes two arguments, start and end, the range of the random number that is generated
print(random.randint(0,2))