
#if something is true, run the code in the indented block
if True:
#>>> code that belongs to an expression must be indented under that expression
    print("A true statement if there ever was one!")
    #if nothing was true, run the else... else always runs if nothing is true!
else:
    print("The first statement must not have been true!")
# declaring a boolean as a variable
#this code does not belong to the else because it is not indented
not_true = False

if not_true:
    print("This will not print")
elif not_true:
    print("neither will this")
elif False:
    print("nor will this")
else:
    print("Since nothing was true, this will print!")


