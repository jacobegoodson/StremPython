# >: greater than
if 1 > 2:
    print("1 is greater than 2")

# <: less than
if 1 < 2:
    print("1 is less than 2")

# ==: is equal to
if 1 == 2:
    print("1 is equal to 2")

# !=: is not equal to
if 1 != 2:
    print("1 does not equal 2")

# >=: greater than or equal to
if 1 >= 2:
    print("1 is greater than or equal to 2")

# <=: less than or equal to
if 2 <= 1:
    print("2 is less than or equal to 1")

# and: and, every expression must be true
if 1 == 1 and 2 == 2:
    print("1 is equal to 1 and 2 is equal to 2")

# or: or, at least one expression must be true, or short circuits
if 1 != 1 or 1 == 1:
    print("1 does not equal 1 or 1 is equal to 1!")

