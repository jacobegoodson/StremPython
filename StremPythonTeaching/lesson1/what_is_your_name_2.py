# Send a string to the console, "Hello there!"
print("Hello there!")
# my_name is a variable
# = means to set the variable to whatever is to the right of the =
# input is a function, it "stops" the program to receive input from the user
my_name = input("What is your name? ")
# The plus sign can be used for string concatenation, joining strings together
print('It is good to meet you, ' + my_name)