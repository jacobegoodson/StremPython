# splicing means dividing up
# strings are actually lists under the hood

# go to the second element/char in the string, grab the rest
# of the string
hi = "hi there"[2:]
print(hi) # " there"

# grab everything up to the 4 element, including the fourth element
hi = "hi there"[:4]
print(hi) # "hi t"

# I am sure you get it by now
hi = "hi there"[3:7]
print(hi) # " ther"