# This is a list, it contains integers
cool_list = [1,2,3]
print(cool_list)

# lists start at 0, not 1
print(cool_list[0])
# 1 is sort of like 2 here:
print(cool_list[1])

# we can add items to a list
cool_list.append(500)
print(cool_list)
# we can also remove items from a list
del cool_list[0]
print(cool_list)
