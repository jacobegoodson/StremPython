
#create a counter so that the loop has a reference of where to stop
counter = 1
# while counter is less than or equal to 10 keep printing the counter
while counter <= 10:
    print(counter)
    # inc the counter by 1
    counter += 1

#reset the counter to 1 to use in another loop
counter = 1

# demonstrate the break statement, it breaks out of a loop no matter what the current
# condition is(true or false)
while counter <= 10:
#>>> notice the indentation
    print(counter)
    counter += 1
    #if the counter is equal to 7 leave the loop
    if counter == 7:
        break
# new function, range, range takes an argument and generates a list from it
range(3) # [0, 1, 2]
# for loop, set i to 1, while i is less than 10, increment i
for i in range(10):
   print(i)

# for loops are very useful, you may be tempted to use while loops for everything but for loops
# are easier to debug and maintain

for i in [1,2,3]:
    print(i)
