import random
#TODO show what students are learning online! gitlab
guess = 0
max_guesses = 6
win = False
name = ""
# generate the random number for the user to guess, from a range of 1 to 20
secret_random_number = random.randint(1,20)

print("Hello! What is your name?")
# Get a string input from the user, presumably their name
name = input()

print("Well, " + name + " I am thinking of a number between 1 and 20")

print("Take a guess as to what the number is: ")

for i in range(max_guesses):
    guess = int(input())
    if guess == secret_random_number:
        #if the guess was correct, the win trigger should be flipped to true
        win = True
        # user has won, break out of the for loop, otherwise the game will keep going
        break

    # let the user know to guess higher
    elif guess < secret_random_number:
        print("Wrong! Maybe you should guess higher...")

    # let the user know to guess lower
    elif guess > secret_random_number:
        print("Wrong! Maybe you should guess lower...")



# if the player guess the right number the win boolean will be true, otherwise the else
# code is run and the player is informed that they lost
if win:
    print("That was correct! You win!")

else:
    print("You have run out of guesses, the secret number was: "
        + secret_random_number +
        "... you lose!")



