# functions allow us to modularize our code, break it up into
# small understandable bits

# each function definition starts with the keyword def
# followed by the name of the function
# finally some parenthesis with the arguments that the function takes
def square(n):
    # indent the code that belongs to the function
    # return is a keyword, it specifies what the function
    # will result in after it has been called
    return n * n

# what do you think x will become?
x = square(3)

# prints the argument given to it, notice there is not return
# the function will return nothing
def print_name(name):
    print(name)

# what do you think x will become?
x = print_name("hi")

# cubes the argument given to it
def cube(n):
    return n * n * n
# prompts user for input, then displays a response with the
# given input
def get_input(message):
    print("please enter some input:")
    inp = input()
    print("your input is: " + inp)

# functions can take multiple arguments, as many as is needed
def multiply(a,b):
    return a * b

# what do you think x will become?
x = multiply(3,4)

# this is the main function, this is where the whole program
# should reside

def main():
    s = square(3)
    c = cube(4)
    mul = multiply(5,6)
    print(str(s) + " " + str(c) + " " + str(mul))

main()