# sanitation is the process of making sure input is correct
# we call this sanitizing the input

def sanitary():
    print("please enter a number from 1 to 5: ")
    # inp must be casted to an integer
    inp = int(input())
    # our condition will not be false until the user has
    # enter a number between 1 and 5, thus the loop will
    # pester the user until they have done as required
    while inp < 1 or inp > 5:
        print("You did not enter a number from 1 to 5!")
        print("Please try again: ")
        inp = int(input())
    print("Thank you for your input =)")

# call the function, we defined it but now we must call it
sanitary()