# Animal is the BASE/PARENT class for all the animal objects
# Methods are functions that belong to a class
class Animal:
    # __init__ is a special method that is called when Animal is
    # constructed/instantiated/reified/realized/created
    # all methods take the class is an argument
    def __init__(self):
        # Animal contains two attributes, which are variables that belong to a class
        self.name = "animal"
        self.health = 10
    # Animal contains a method, a function that belongs to a class
    # the method is speak
    def speak(self):
        print(self.name + " makes a sound!")

# All the following classes are SUBCLASSES/CHILDREN of Animal
# they EXTEND or are DERIVED from Animal
# this means that they INHERIT all the methods and attributes from Animal
# to extend a class we place the class in the parenthesis of the definition,
# like an argument to a function
class Cat(Animal):
    # speak OVERRIDES the base class method speak from Animal, it adds an
    # additional argument to speak
    def speak(self, meow):
        print(meow + "MEEEEOOOOWW")

class Dog(Animal):
    # speak OVERRIDES the base class method speak from Animal
    def speak(self):
        print("WOOF!")

class Bat(Animal):
    # When Bat is instantiated it overrides the attribute name
    def __init__(self):
        # can call the parent constructor like so super().__init__()
        self.name = "Bat"


# Each object contains the same method speak, but speak
# acts differently based on the object it was invoked upon
# this is called POLYMORPHISM
bat = Bat()
bat.speak()
cat = Cat()
cat.speak("cat says... ")
dog = Dog()
dog.speak()